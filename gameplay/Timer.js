import React, {useEffect} from 'react';
import {View, StyleSheet, Text, Alert} from 'react-native';
import {connect} from 'react-redux';
import Colors from '../constants/Colors';
import {
  GameRoundTimer,
  GameOver,
  // QuitGame,
  // PlayAgain,
} from '../redux/actions/Handlers';

const GameTimer = props => {
  let counter;
  useEffect(() => {
    if (props.gameStarted) {
      counter = setTimeout(() => {
        props.counterHandler(props.roundTime);
      }, 1000);
    }
    if (props.roundTime - 1 < 0) {
      clearTimeout(counter);
      Alert.alert('Game Over', `Answer is ${props.answer}`, [
        {
          text: 'Quit Game',
          style: 'default',
          onPress: () => props.quitGame(),
        },
        {
          text: 'Play Again',
          onPress: () => props.playAgain(props.initialRoundTime,3),
        },
      ]);
      return;
    }
    if (props.answerChosen) {
      clearTimeout(counter);
    }
  }, [props.roundTime]);

  return (
    <View
      style={[
        styles.timeContainer,
        {backgroundColor: props.roundTime <= 5 ? 'red' : 'white'},
      ]}>
      <Text
        style={[
          styles.time,
          {color: props.roundTime <= 5 ? 'white' : Colors.primary},
        ]}>
        {props.roundTime}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  timeContainer: {
    borderWidth: 2,
    borderColor: 'grey',
    // backgroundColor:'white',
    borderRadius: 10,
    padding: 26,
    width: 100,
    alignItems: 'center',
  },
  time: {
    // color:Colors.primary,
    fontWeight: 'bold',
    fontSize: 30,
  },
});

const MapState = state => {
  return {
    // initialRoundTime: state.Settings.initialRoundTime,
    roundTime: state.Settings.roundTime,
    gameStarted: state.Settings.startGame,
  };
};

const MapDispatch = dispatch => {
  return {
    counterHandler: counter => dispatch(GameRoundTimer(counter)),
    roundTimeUp: ind => dispatch(GameOver(ind)),
    //quitGame: (RT) => dispatch(QuitGame(RT)), //RT = initial ROund TIme
    // playAgain: (initialTime) => dispatch(PlayAgain(initialTime)),
  };
};

export default connect(
  MapState,
  MapDispatch,
)(GameTimer);
