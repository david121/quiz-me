import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import TouchableStyle from '../constants/Touchables';

const LifeLines = props => {
  let extras = null;
  if (props.gameStarted && !props.answered) {
    extras = (
      <View style={{...props.style}}>
        <TouchableOpacity style={TouchableStyle.lifeLine} onPress={()=>props.fiftyHandler(props.questionObject)}>
          <Text>50:50</Text>
        </TouchableOpacity>
        <TouchableOpacity style={TouchableStyle.lifeLine}>
          <Text>Hint</Text>
        </TouchableOpacity>
      </View>
    );
  }else if (props.gameStarted && props.answered && props.answerCorrect) {
    extras = (
        <View style={{...props.style}}>
          <TouchableOpacity style={TouchableStyle.lifeLine}>
            {/* <Text>Quit</Text> */}
          </TouchableOpacity>
          <TouchableOpacity style={TouchableStyle.lifeLine} onPress={()=>props.next(props.initialTime)}>
            <Text>Next</Text>
          </TouchableOpacity>
        </View>
      );
  } else if (props.answered && !props.answerCorrect) {
    extras = (
        <View style={{...props.style}}>
        <TouchableOpacity style={TouchableStyle.lifeLine} onPress={props.quitGame}>
          <Text>Quit Game</Text>
        </TouchableOpacity>
        <TouchableOpacity style={TouchableStyle.lifeLine} onPress={()=>{props.playAgain(props.initialTime,3)}}>
          <Text>Retry</Text>
        </TouchableOpacity>
      </View>
    );
  }
  return extras;
};

const styles = StyleSheet.create({});

export default LifeLines;
