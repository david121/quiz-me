import React, {useEffect} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {connect} from 'react-redux';
import Colors from '../constants/Colors';
const Question = props => {
  useEffect(()=>{
    // console.log(props.questions[0].question)
  });
  return (
    <View style={{...styles.questionContainer,...props.style}}>
      <Text style={styles.questionText}>{props.question}</Text>
    </View>
  );

};

const styles = StyleSheet.create({
    questionContainer:{alignItems:'center',margin:10,padding:10},
    questionText : {textAlign:'center',fontWeight:'bold', fontFamily:'serif',},
});

const MapState = state => {
  return {
    questions : state.Questions.questions
  };
};

export default connect(
  MapState,
  null,
)(Question);
