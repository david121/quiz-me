import React, {useEffect} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import Colors from '../constants/Colors';
import {StartGameCountDown, DecrementStartGame} from '../redux/actions/Handlers';

const StartGameButton = props => {
    let countdown;
    useEffect(()=>{
        if (props.startGameCD) {
            countdown = setTimeout(()=>{props.Decrement(props.startGameText)},1000)
        }
    })

  return (
    <View style={styles.startGameContainer}>
      <TouchableOpacity
        style={styles.startGameBtn}
        disabled={props.startGameCD}
        onPress={()=>props.startGameCountHandler(3)}>
        <Text style={styles.text}>{props.startGameText}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  startGameContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  startGameBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 100,
    borderColor: Colors.secondary,
    elevation: 5,
    padding: 5,
    height: 180,
    width: 180,
    backgroundColor: Colors.primary,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 28,
    fontFamily: 'serif',
    color: 'white',
  },
});

const MapState = state => {
  return {
    startGameText: state.Settings.startGametext,
    startGameCD: state.Settings.startGameCountDown,
  };
};
const MapDispatch = dispatch => {
  return {
    startGameCountHandler: (count) => dispatch(StartGameCountDown(count)),
    Decrement: (count) => dispatch(DecrementStartGame(count)),
  };
};

export default connect(
  MapState,
  MapDispatch,
)(StartGameButton);
