import React, {useEffect} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import TouchableStyle from '../constants/Touchables';

const Answer = props => {
  
  let options = props.options.map((item, index) => {
    let stylesList = [TouchableStyle.button, TouchableStyle.answerBtn];
    if (props.isAnswered) {
        stylesList[stylesList.length] = {borderWidth:0.5, borderColor:'red'}
        if (props.choiceIndex === index && props.correctlyAnswered) {
          stylesList.pop();
          stylesList.push({borderWidth:3, borderColor:'green'})
        }else if (props.choiceIndex === index && !props.correctlyAnswered) {
          stylesList.pop();
          stylesList.push({borderWidth:3, borderColor:'red'})          
        }
        if (props.choiceIndex !== index && item === props.answer) {
          stylesList.pop();
          stylesList.push({borderWidth:3, borderColor:'green'})
        }
    }
    return (
      <TouchableOpacity
        key={index}
        style={stylesList}
        onPress={()=>props.onAnswerPress(item,props.answer,index)}
        disabled={props.roundTime === 0 || props.isAnswered}>
        <Text style={TouchableStyle.answerText}>{props.fiftyFifty.includes(item) ? '' : item}</Text>
      </TouchableOpacity>
    );
  });

  return (
    <View style={{...styles.answerContainer, ...props.style}}>
      {options}
    </View>
  );
};

const styles = StyleSheet.create({
  answerContainer: {
    alignItems: 'center',
  },
});

export default Answer;
