import React, {useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import Header from '../components/Header';
import Questions from './Questions';
import Answer from './Answer';
import Timer from './Timer';
import Card from '../components/Card';
import StartGameButton from './StartGameButton';
import {FetchQuestion, PlayAgain, QuitGame} from '../redux/actions/Handlers';
import {choiceHandler, nextQuestion, fiftyFifty} from '../redux/actions/GamePlayHandler';
import LifeLines from './LifeLines';

const GameScreen = props => {
  useEffect(() => {
    if (props.questions.length < 0) {
      //props.quitGame();
    }else
    if (props.gameStartCD ) {//&& props.questions.length > 0
      // fetch first question when ready button clicked
      props.getQuestion(0, props.questions.length, props.questions);
    }
    return () => {
      //console.log('finished');
    };
  }, [props.gameStartCD]);

  return (
    <View style={styles.gameScreen}>
      {props.gameStarted ? (
        <View style={{flex: 2}}>
          <Header style={styles.header}>
            <Timer
              answer={props.currentQuestion.answer}
              answerChosen={props.answerChosen}
              initialRoundTime={props.initialRoundTime}
              playAgain = {props.playAgain}
              quitGame = {props.quitGame}
            />
          </Header>

          <Card style={styles.question}>
            <Questions question={props.currentQuestion.question} />
            <LifeLines
              style={styles.lifeLines}
              gameStarted={props.gameStarted}
              answered={props.answerChosen}
              answerCorrect={props.correctlyAnswered}
              next = {props.nextQuestion}
              initialTime={props.initialRoundTime}
              playAgain = {props.playAgain}
              quitGame = {props.quitGame}
              questionObject = {props.currentQuestion}
              fiftyHandler = {props.fiftyHandler}
            />
          </Card>

          <Answer
            style={styles.answer}
            roundTime={props.roundTime}
            options={props.currentQuestion.options}
            answer={props.currentQuestion.answer}
            onAnswerPress={props.userChoice}
            isAnswered={props.answerChosen}
            choiceIndex={props.choiceIndex}
            correctlyAnswered={props.correctlyAnswered}
            fiftyFifty = {props.fiftyFiftyOptions}
          />
        </View>
      ) : (
        <View style={{flex: 1}}>
          <StartGameButton />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  gameScreen: {flex: 1},
  header: {flex: 1.5},
  question: {flex: 2, width: 400, margin: 5},
  answer: {flex: 3, width: '100%'},
  lifeLines: {
    flexDirection: 'row',
    position: 'absolute',
    justifyContent: 'space-between',
    bottom: 0,
    width: '100%',
    height: 40,
    borderWidth: 3,
    borderTopColor: 'purple',
    borderBottomWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    // backgroundColor: Colors.secondary,
    //    padding:20,
  },
});

const MapState = state => {
  return {
    gameStarted: state.Settings.startGame,
    gameStartCD: state.Settings.startGameCountDown,
    roundTime: state.Settings.roundTime,
    initialRoundTime: state.Settings.initialRoundTime,
    questions: state.Questions.questions,
    currentQuestion: state.Questions.currentQuestion,
    answerChosen: state.Questions.answerChosen,
    choiceIndex: state.Questions.userAnswerIndex,
    correctlyAnswered: state.Questions.correctlyAnswered,
    fiftyFiftyOptions: state.Questions.fiftyFiftys,
  };
};
const MapDispatch = dispatch => {
  return {
    getQuestion: (min, max, questions) =>
      dispatch(FetchQuestion(min, max, questions)),
    userChoice: (choice, answer, choiceIndex) => {
      dispatch(choiceHandler(choice, answer, choiceIndex));
    },
    nextQuestion : (time) => { dispatch(nextQuestion(time))},
    playAgain: (initialTime,countDown) => dispatch(PlayAgain(initialTime,countDown)),
    quitGame: (RT) => dispatch(QuitGame(RT)),
    fiftyHandler : (questionObject) => dispatch(fiftyFifty(questionObject))
  };
};

export default connect(
  MapState,
  MapDispatch,
)(GameScreen);
