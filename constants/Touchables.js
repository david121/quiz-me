import Colors from './../constants/Colors';

const TouchableStyle = {
    button:{
        width: "75%",
        justifyContent: "center",
        alignItems: "center",
        padding: 10,
        backgroundColor: Colors.secondary,//'white'
        height: 35,
        marginVertical: 10,  
    },
    settingsBtn:{ //other style for settings buttons
        borderEndWidth: 2,
        borderEndColor: Colors.primary,
        borderBottomEndRadius: 15,
    },
    answerBtn : { //Game Answer button
        borderRadius:10,
    },
    answerText:{
        fontWeight:'bold',
        fontFamily:'serif'
    },
    text:{
        color: "#4d3546",
    },
    bacButton:{
        alignItems: "center",
        width:150,
        padding: 10,
        backgroundColor: Colors.primary,
        marginTop:40,
    },
    backButtonText:{
        color:'white'
    },

    //Game LifeLines
    lifeLine :{
        width:'40%',
        // backgroundColor:Colors.secondary,
        borderRadius:10,
        marginBottom:8,
        justifyContent:'center',
        alignItems:'center',
        // borderWidth: 1.5,
        // borderColor: Colors.primary,
        
    }
}

export default TouchableStyle;