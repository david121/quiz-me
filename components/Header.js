import React from 'react';

import {StyleSheet,View} from 'react-native';
import {connect } from 'react-redux';
import Colors from '../constants/Colors'

const Header = props =>{
    //{...styles.header,...props.style}
    return (
        <View style={[styles.header,props.style]}>{props.children}</View>
    )
}

const styles = StyleSheet.create({
    header:{
        width:'100%',
        backgroundColor:Colors.primary,
        justifyContent:'center',
        alignItems:'center',
    }
})

const MapState = (state) =>{
    return {

    }
}

const MapDispatch = (dispatch) =>{
    return {
        
    }
}

export default connect(null,null)(Header);