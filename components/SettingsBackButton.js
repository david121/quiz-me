import React from 'react';
import TouchableStyle from '../constants/Touchables';
import {View, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {BackBtn} from '../redux/actions/Handlers';
const BackButton = props => {
  return (
    <View>
      <TouchableOpacity style={TouchableStyle.bacButton} onPress={props.Back}>
        <Text style={TouchableStyle.backButtonText}>{'<<'+props.difficulty}</Text>
      </TouchableOpacity>
    </View>
  );
};

const MapState = state => {
  return {
    difficulty: state.Settings.difficulty,
  };
};

const MapDispatch = dispatch => {
  return {
    Back: () => {
      dispatch(BackBtn());
    },
  };
};

export default connect(
  MapState,
  MapDispatch,
)(BackButton);
