import {AllQuestions, AnsweredQuestions} from '../../gameplay/QuestionsSorter'
const initialState = {
    questions: AllQuestions,
    answeredQuestions :[],//AnsweredQuestions
    currentQuestion : {},
    fiftyFiftys : [],
    
    /*answer props*/
    answerChosen : false,
    userAnswer : null,
    userAnswerIndex : null,
    correctlyAnswered : false // answered correctly?
}

const QuestionsReducer = (state = initialState,actions) =>{
    switch (actions.type) {
        case 'NEW_QUESTION':
            //console.log(actions.questions.length)
            return{
                ...state,
                currentQuestion: actions.question[0],
                questions : actions.remQuestions,
                userAnswer:null,
                correctlyAnswered: false,
            }
        case 'CHOICE_MADE':
            return{
                ...state,
                answerChosen:actions.choice,
                correctlyAnswered: actions.answered,// answered Correctly?
                userAnswerIndex : actions.choiceIndex
            }
        case 'RETRY':
            return{
                ...state,
                answerChosen:false,
                questions: AllQuestions,
                answeredQuestions :[],
                currentQuestion : {},
                fiftyFiftys : [],
            }
        case 'FIFTY_FIFTY':
            return{
                ...state,
                fiftyFiftys: actions.randoms,
            }
        case 'QUIT':
            return{
                ...state,
                answerChosen:false,
                answerChosen : false,
                userAnswer : null,
                userAnswerIndex : null,
                correctlyAnswered : false,
                questions: AllQuestions,
                answeredQuestions :[],
                currentQuestion : {},
            }
        default: return state;
    }
}

export default QuestionsReducer;