
const intialState = {
    difficulty : null,
    initialRoundTime : null,
    roundTime : null,
    startGametext : 'Start Game', 
    startGameCountDown : false, 
    startGame : false,
}

const SettingsReducer = (state = intialState, actions) =>{
        switch (actions.type) {
            case 'DIFFICULTY':
                return{
                    ...state,
                    difficulty:actions.value
                }
            case 'BACK':
                return{
                    ...state,
                    difficulty:null
                }
            case 'ROUND_TIME':
                return{
                    ...state,
                    initialRoundTime:actions.time,
                    roundTime:actions.time,
                }
            case 'START_GAME':
                return{
                    ...state,
                    startGame:true,
                    startGameCountDown: false,
                }
            case 'START_GAME_COUNTDOWN':
                return{
                    ...state,
                    startGameCountDown: true,
                    startGametext: actions.count
                }
            case 'DECREMENT_START_CD':
                return{
                    ...state,
                    startGametext: state.startGametext -1,
                }
            case 'GAME_READY':
                return{
                    ...state,
                    startGametext: 'READY!'
                }
            
            case 'DECREMENT':
                return{
                    ...state,
                    roundTime: state.roundTime -1,
                }

            case 'RETRY':
                return{
                    ...state,
                    startGame: false,
                }
            case 'NEXT_QUESTION':
                return{
                    ...state,
                }

            case 'QUIT':
                return{
                    ...state,
                    difficulty:null,
                    roundTime:null,
                    startGameCountDown:false,
                    startGame:false,
                    startGametext:"Start Game"}
            default: return state;
        }
}

export default SettingsReducer;