// import {CONTROL_SCREEN as SCREEN} from '../actions/Type'
const initialState = {
    screen: "Settings"
}

const ScreensReducer = (state = initialState, actions) =>{
    switch (actions.type) {
        case "SCREEN":
            return{
               ...state,
               screen:actions.value     
            }
    
        default: return state;
    }
}

export default ScreensReducer;