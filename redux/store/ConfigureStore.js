import {combineReducers, createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import ScreensReducer from '../reducers/ScreensReducer';
import SettingsReducer from '../reducers/SettingsReducer';
import QuestionsReducer from '../reducers/QuestionsReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const reducers = combineReducers({
    Screens: ScreensReducer,
    Settings : SettingsReducer,
    Questions : QuestionsReducer
});
const GlobalStore = () => createStore(reducers,composeEnhancers(applyMiddleware(thunk)));

export default GlobalStore;
