import {PlayAgain} from './Handlers'

export const choiceHandler = (choice, answer, choiceIndex) => {
  return dispatch => {
      let answered = choice === answer;
    //   console.log(answered);
    dispatch({
            type: 'CHOICE_MADE',
            choice, answered, choiceIndex
        });
  };
};

const removeIncorrectAnswer = (options,answer) =>{
    return options.filter(option=>option !== answer )
}

export const fiftyFifty = (questionObject) => {
    return dispatch =>{
      let incorrects = removeIncorrectAnswer(questionObject.options, questionObject.answer);
      let twoIncorrects = [];
      let randAns;
      for( let i = 0; i < 2 ; i++){
          randAns = Math.floor(Math.random() * (incorrects.length));
          twoIncorrects.push(incorrects.filter((ans,ind) => ind === randAns)[0]);
          incorrects = incorrects.filter((item, ind)=> ind !== randAns);
      }
      dispatch({
        type : "FIFTY_FIFTY",
        randoms : twoIncorrects 
      })
    }
}

export const nextQuestion = (roundTime,questions) => {
  return dispatch => {
    dispatch(PlayAgain(roundTime,1));
     dispatch({ type: 'NEXT_QUESTION',})
  };
};
