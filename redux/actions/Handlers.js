
export const SetDifficultyHandler = (selection) =>{
    return dispatch => { dispatch({ type: "DIFFICULTY", value:selection }) }
}

export const SetRoundTimeHandler = (time) =>{
    return dispatch =>{     dispatch({ type:"ROUND_TIME",  time })}
}

export const GameRoundTimer = () =>{
    return dispatch =>{ dispatch({  type:"DECREMENT" }) }
}

export const BackBtn = () =>{
    return dispatch =>{ dispatch({type:"BACK"}) }
}

export const GameOver = () => {
    return dispatch => {dispatch({ type: "TIME_UP" })   }
}

export const QuitGame = () => {
    return dispatch => {dispatch({ type: "QUIT"})}}

export const PlayAgain = (initialTime,countDownTime) => {
    return dispatch => {
        dispatch(SetRoundTimeHandler(initialTime));
        dispatch(StartGameCountDown(countDownTime));
        dispatch({type: "RETRY"})
    }
}

export const StartGameCountDown = (count) =>{
    return dispatch =>{dispatch ({type:"START_GAME_COUNTDOWN",count})}
};

export const DecrementStartGame = (count) =>{
    return dispatch =>{
        if (count -1 === 0) {
            dispatch({type:"GAME_READY"})
            return;
        }
        if (count === "READY!") {
            dispatch({type:"START_GAME"})
            return;
        }
        dispatch ({
            type:"DECREMENT_START_CD"
        })
    }
};

const shuffleOptions = options => {
    let i, j, k;
      for (i = options.length -1; i > 0; i--) {
        j = Math.floor(Math.random() * i)
        k = options[i]
        options[i] = options[j]
        options[j] = k
      }
      return options;
    }

export const FetchQuestion = (min,max,questions) =>{
        
    let questionNumber = Math.floor(Math.random() * (max - min)) + min;
    let question = questions.filter((question,index)=>index === questionNumber && !question.answered);
    question.options = shuffleOptions(question[0].options);

    let updateQuestions = questions.filter( ques => ques.id !== question[0].id);

    return dispatch =>{
        dispatch({
            type: "NEW_QUESTION",
            question,
            remQuestions:updateQuestions
        })
    }
}