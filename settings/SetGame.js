import React,{useEffect} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {connect} from 'react-redux';

import Card from '../components/Card';
import Difficulty from './SetDifficulty';
import GameTime from './SetTimer';
import BackButton from '../components/SettingsBackButton';

const SetGame = props => {
 
  let backBtn = props.difficulty ? <BackButton/> : null;

  return (
    <View style={styles.root}>
      <View style={styles.settingPhase}>
        <Text style={styles.settingDescription}>{props.title}</Text>
      </View>
      <Card style={styles.card}>
       { !props.difficulty ? <Difficulty /> : <GameTime/>}
      </Card>
      {backBtn}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 2,
    width: '100%',
    alignItems: 'center',
  },
  card: {
    marginTop: 5,
    width: 300,
    padding: 10,
    maxWidth: '80%',
  },
  settingPhase: {},
  settingDescription: {
    fontSize: 18,
    padding: 15,
    fontWeight: 'bold',
  },
});

const MapState = (state) =>{
  return {
      difficulty : state.Settings.difficulty,
  }
}

export default connect(
  MapState,
  null,
)(SetGame);
