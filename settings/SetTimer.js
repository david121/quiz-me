import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import TouchableStyle from '../constants/Touchables';
import { connect } from 'react-redux';
import { SetRoundTimeHandler } from '../redux/actions/Handlers';
const SetTimer = props => {
  return (
    <View style={styles.root}>
      <TouchableOpacity style={TouchableStyle.button} onPress={()=>props.roundTimeHandler(15)}>
        <Text style={TouchableStyle.text}>15 Seconds</Text>
      </TouchableOpacity>
      <TouchableOpacity style={TouchableStyle.button} onPress={()=>props.roundTimeHandler(30)}>
        <Text style={TouchableStyle.text}>30 Seconds</Text>
      </TouchableOpacity>
      <TouchableOpacity style={TouchableStyle.button} onPress={()=>props.roundTimeHandler(60)}>
        <Text style={TouchableStyle.text}>1 Minute</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    padding: 10,
    width: '100%',
    alignItems: 'center',
  },
});

const MapDispatch = (dispatch)=>{
  return {
      roundTimeHandler : (time) =>{dispatch(SetRoundTimeHandler(time))}
  }
}

export default connect(null,MapDispatch)(SetTimer);
