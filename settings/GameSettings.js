import React from 'react';

import {StyleSheet, View, Text} from 'react-native';
import {connect} from 'react-redux';
import Header from '../components/Header';
import SetGame from './SetGame';
const GameSettings = props => {
  return (
    <View style={{...props.style,...styles.settingsWrapper}}>
      <Header style={styles.header}><Text style={styles.HeaderText}>Quiz Me</Text></Header>
      <SetGame title={!props.difficulty ? 'Difficulty' : 'Round Time'}/>
    </View>
  );
};

const styles = StyleSheet.create({
    header:{
        flex:1
    },
    settingsWrapper:{
        flex:1,
        alignItems:'center'
    },
    HeaderText:{
      color:'white',
      fontSize:35,
      fontWeight:"bold"
    }
});

const MapState = (state) =>{
  return {
      difficulty : state.Settings.difficulty,
  }
}
const MapDispatch = dispatch => {
  return {};
};

export default connect(
  MapState,
  null,
)(GameSettings);
