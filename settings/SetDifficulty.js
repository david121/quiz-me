import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';

import TouchableStyle from '../constants/Touchables'
import {SetDifficultyHandler} from '../redux/actions/Handlers'

const SetDifficulty = props => {
  return (
    <View style={styles.root}>
      <TouchableOpacity style={[TouchableStyle.button,TouchableStyle.settingsBtn]} onPress={()=>props.difficultyHandler('Easy')}>
        <Text style={TouchableStyle.text}>Piece of Cake</Text>
      </TouchableOpacity>
      <TouchableOpacity style={[TouchableStyle.button,TouchableStyle.settingsBtn]} onPress={()=>props.difficultyHandler('Medium')}>
        <Text style={TouchableStyle.text}>Testing</Text>
      </TouchableOpacity>
      <TouchableOpacity style={[TouchableStyle.button,TouchableStyle.settingsBtn]} onPress={()=>props.difficultyHandler('Hard')}>
        <Text style={TouchableStyle.text}>No Go</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
    root : {
        padding:10,
        width:'100%',
        alignItems:"center"
    },
});

const MapDispatch = (dispatch) =>{
  return {
      difficultyHandler : (diff) => {dispatch(SetDifficultyHandler(diff))}
  }
}
export default connect(
  null,
  MapDispatch,
)(SetDifficulty);
