
import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import {connect} from 'react-redux';

import GameSettings from './settings/GameSettings';
import GameScreen from './gameplay/GameScreen';

const App = props => {

  useEffect(()=>{
    // console.log(props.settings);
  })

  return (
    <>
      <View style={styles.container}>
       { props.settings.difficulty && props.settings.roundTime !== null ? <GameScreen/> : <GameSettings/>}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flex: 1,
  },
});

const MapState = (state) =>{
  return {
    screen : state.Screens.screen,
    settings : state.Settings
  }
}

export default connect(MapState,null)(App);
